package com.example.tableview;

import android.os.AsyncTask;
import android.util.Log;

import com.example.tableview.obj.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ConnectionRest extends AsyncTask<String, Void, String> {
    //private final static String URL = "https://api.munier.me/gr0/product/";
    private final static String URL = "https://api.munier.me/jwt/";
    private JSONObject jsonObj = null;
    private String action = "product", token = null;

    @Override
    protected String doInBackground(String... strings) {
        try {
            /*jsonObj = new JSONObject();
            jsonObj.put("name", "carrotte");
            jsonObj.put("type", "legume");
            jsonObj.put("price", 2.1);

            Log.v("id ", get("POST"));*/

            /*List<Product> listProduct = parse(get("GET"));
            for (int i=0;i<listProduct.size();i++){
                Log.v("PRODUIT ", listProduct.get(i).getName()+" "+listProduct.get(i).getType()+" "+listProduct.get(i).getPrice()+"\n");
            }*/

            return get(strings[0]);

        }catch (IOException | JSONException e){
            e.printStackTrace();
        }
        return null;
    }

    public String get(String methode) throws IOException, JSONException {
        String url = URL;
        InputStream is = null;
        String parameters = "";
        Log.v("methode", methode);
        if(!methode.equals("POST")&&(jsonObj!=null)){
            url += jsonObj.getInt("id");
        }
        if(jsonObj != null){
            if(methode.equals("PUT")){
                jsonObj.remove("id");
            }
            parameters  = "data="+ URLEncoder.encode(jsonObj.toString(), "utf-8");
            Log.v("URL", url+" "+parameters);
        }
        try {
            final HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setRequestMethod(methode);

            if (token != null){
                conn.setRequestProperty("Authorization", "Bearer"+URLEncoder.encode(token, "UTF-8"));
            }

            // Pour les methode POST et PUT on envoie les parametre avec l'OutputStreamWriter
            if(methode.equals("POST")||methode.equals("PUT")){
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
                out.write(parameters);// here i sent the parameter
                out.close();
            }else{
                conn.setDoInput(true);
                conn.connect();
            }

            is = conn.getInputStream();
            // Lit le InputStream et l'enregistre dans une string
            return readIt(is);

        } finally {
            // Pour etre sur que le InputStream soit ferme apres avoir quitter l'application
            if (is != null) {
                is.close();
            }
        }
    }

    private String readIt(InputStream is) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder response = new StringBuilder();
        String line;
        while ((line = r.readLine()) != null) {
            response.append(line).append('\n');
        }
        return response.toString();
    }

    public ArrayList<Product> parse(String json) {
        try {
            ArrayList products = new ArrayList<>();
            JSONArray jProductArray = new JSONArray(json);
            for (int i = 0; i < jProductArray.length(); i++) {
                products.add(new Product(jProductArray.optJSONObject(i)));
            }
            return products;
        } catch (JSONException e) {
            Log.v("TAG","[JSONException] e : " + e.getMessage());
        }
        return null;
    }

    public void setJsonObj(JSONObject jsonObj) {
        this.jsonObj = jsonObj;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public void setToken(String token) {
        this.token = token;
    }

    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
