package com.example.tableview.obj;

import org.json.JSONObject;

public class Product {
    private int id;
    private String name;
    private String type;
    private double price;

    public Product(JSONObject jObject) {
        this.id = jObject.optInt("id");
        this.name = jObject.optString("name");
        this.type = jObject.optString("type");
        this.price = jObject.optDouble("price");
    }

    public int getId() { return id; }
    public String getName() { return name; }
    public String getType() { return type; }
    public double getPrice() { return price; }
}
